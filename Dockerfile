FROM image-registry.openshift-image-registry.svc:5000/odh/s2i-lab-elyra:v0.0.9

ARG PACH_VERSION=2.1.4
ARG PACH_RELEASES=https://github.com/pachyderm/pachyderm/releases/download

RUN curl -L -o /tmp/pachctl.tar.gz ${PACH_RELEASES}/v${PACH_VERSION}/pachctl_${PACH_VERSION}_linux_amd64.tar.gz && \
    tar -xvf /tmp/pachctl.tar.gz -C /tmp && \
    mkdir -p ${HOME}/.local/bin && \
    install /tmp/pachctl_${PACH_VERSION}_linux_amd64/pachctl ${HOME}/.local/bin
